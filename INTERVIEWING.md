# Interviewing Guide

👋 If you're reading this, you probably have an interview scheduled with me.
Thank you for making the time. I am looking forward to talking with you and learning more about you.

This document is intended to give context and set expectations for our conversation.
I know that interviews can be stressful, so I hope giving you as much information as I can will put you at ease and set you up for success.

## Before our conversation

- Please **find a time that is good for you.** 🌏

    Timezones and scheduling can be hard.
    If there isn't a time that's convenient for you, just ask.
    There is absolutely no good reason to schedule yourself an interview at a time that isn't comfortable for you (e.g. midnight or 5AM).

- Please email and **ask if you need reschedule.** 📅

    There is absolutely no ill consequence to rescheduling our conversation or asking for more time.
    If family duties or something unexpected is calling for your attention please do not hesitate to reschedule.

- Please prepare by **verifying Zoom is in working order.** 🎤

    Our conversation will likely be over Zoom. Please take some time to test your setup beforehand.

## During our conversation

- I will be taking notes. ✍️

    If you hear me typing, it's not because I'm answering email or am on Slack - you have my undivided attention.

- I will **not** ask trick questions. 🎩

    I want to see you at your best, and will try to make the interview as comfortable as possible.
    If you need to take a moment to think, or ask me to clarify I won't "take points off".

- I may interrupt you. ⏩

    An hour is a short amount of time to get to know you.
    To make sure we have an efficient conversation, I may interrupt.

- I will answer your questions. 🙋‍

    I will always leave time for your questions at the end of the conversation.
    Please ask the questions that you actually want the answers to, not the questions you imagine I want to hear.
    Interviewing is a two-way street; it's just as important that this is a fit for you as it is the other way around.

## After our conversation

- Don't feel obliged to send me an email.

    I think a generic thank-you email is a formality with no real value.
    Thank you for dedicating the time and energy to apply and interview.

    However, please do feel free to follow up if you'd like to emphasize something I should know, something important you forgot to mention, or want to follow up on something we talked about.

Thank you again for taking the time to talk with me!
I really appreciate you and your time, and am looking forward to our conversation! 🙌
