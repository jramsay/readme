# README

Hi, I’m James. At Remote, I am currently Director of Product. This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team. Please feel free to contribute to this page by opening a merge request.

## About me

- I grew up in regional Victoria, about an hour from Melbourne – where I now live. I recently moved back from New York City to be closer to family and have children.
- While studying computer science at The University of Melbourne, I stumbled into product management while working on a software startup. I learnt the hard way that customers can request solutions that are quite unrelated to the underlying problems preventing adoption.
- I enjoy learning how things work, and sharing what I learn with others.
  - [Enneagram Type 5](https://www.integrative9.com/enneagram/introduction/type-5/)
- I enjoy making things and spending time in the outdoors cycling gravel trails, gardening, and woodworking. Mostly I ride around with my son in our [cargo bike](https://jramsay.com.au/2021/06/07/groceries/) these days, rather than adventure cycling.

## My role as Director of Product

## How you can help me

## My working style

